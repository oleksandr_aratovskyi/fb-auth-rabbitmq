<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\RegisterRequest;
use App\Jobs\SendRegisterEmail;
use App\User;
use App\Http\Controllers\Controller;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Store a new user in database
     *
     * @param array $data
     *
     * @return User
     */
    protected function addUser($data){
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        if(isset($data['fb_id'])){
            $user->fb_id = $data['fb_id'];
            $user->save();
        }

        return $user;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param RegisterRequest $request
     * @return User
     */
    public function store(RegisterRequest $request)
    {
        $data = $request->toArray();
        $user = $this->addUser($data);

        $this->dispatch(new SendRegisterEmail($user));
        session()->flash('success', 'User successfully created');
        return redirect()->route('auth.register');
    }

    /**
     * Render registration page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('auth.register');
    }

    /**
     * Redirect to facebook auth link
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fbLogin()
    {
        $fb = new Facebook();
        $helper = $fb->getRedirectLoginHelper();
        $loginUrl = urldecode($helper->getLoginUrl(env('FB_REDIRECT_URI'), ['email']));

        return redirect()->to($loginUrl);
    }

    /**
     * Process registration after facebook callback
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function fbCallback()
    {
        $appId = env('FB_CLIENT_ID');
        $fb = new Facebook();
        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch (FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (!isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($appId); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();

        if (!$accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }
        }

        try {
            $response = $fb->get('/me?fields=id,name,email', $accessToken->getValue());
        } catch(FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $user = $response->getGraphUser();

        $existingUser = User::query()->where(function(Builder $q) use ($user){
            return $q->orWhere('fb_id', $user['id'])->orWhere('email', $user['email']);
        });

        if($existingUser->exists()){
            session()->flash('error', 'User already exists');
            return redirect()->route('auth.register');
        }

        $password = random_int(100000, 999999);
        $user = $this->addUser([
            'fb_id' => $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'password' => $password
        ]);

        session()->flash('success', 'User successfully created');
        $this->dispatch(new SendRegisterEmail($user, $password));
        return redirect()->route('auth.register');
    }
}

Hi, {{ $name }}!<br><br>

You are registered on Site.com!<br><br>

Your credentials:<br>
Login: {{ $email }}<br>
Password: {{ $reg_password }}<br><br>

Best Regards,<br>
Site.com team